const int CONNEXION = 13;
int ledPin = 9;
int red1 = 2;
int orange1 = 3;
int green1 = 4;
int red2 = 5;
int orange2 = 6;
int green2 = 7;

void setup()
{
  setTrafficLed();
  setSerial();
  lightOffAll();
}

void loop()
{
  //blinkLed(1000);
  //mesureTimeElapsed();
  trafficLight();
  //lightOnAll();
}

void setSerial()
{
  Serial.begin(9600);
  Serial.println("DEBUT");
}

void setTrafficLed()
{
  setLed(red1);
  setLed(orange1);
  setLed(green1);
  setLed(red2);
  setLed(orange2);
  setLed(green2);
}

void setLed(int led)
{
  pinMode(led, OUTPUT);
}

void blinkLed(int delayNumber, int led)
{
  digitalWrite(led,HIGH);
  delay(delayNumber);
  digitalWrite(led,LOW);
  delay(delayNumber);
}

void mesureTimeElapsed()
{
  unsigned long tempsFin;
  unsigned long tempsDepart = micros();

  blinkLed(1000, ledPin);

  tempsFin = micros();
  Serial.println(tempsFin - tempsDepart);
}

void lightOn(int led)
{
  digitalWrite(led,LOW);
}

void lightOff(int led)
{
  digitalWrite(led,HIGH);
}

void lightOnAll()
{
  lightOn(red1);
  lightOn(red2);
  lightOn(green2);
  lightOn(green1);
  lightOn(orange2);
  lightOn(orange1);
}

void lightOffAll()
{
  lightOff(red1);
  lightOff(red2);
  lightOff(green2);
  lightOff(green1);
  lightOff(orange2);
  lightOff(orange1);
}

void trafficLight()
{
  lightOn(red1);
  lightOn(green2);
  delay(3000);
  lightOn(orange2);
  lightOff(green2);
  delay(1000);
  lightOn(red2);
  lightOff(orange2);
  delay(1000);
  lightOn(green1);
  lightOff(red1);
  delay(3000);
  lightOffAll();
}


